# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141126193147) do

  create_table "appointments", force: true do |t|
    t.integer  "appt_id"
    t.integer  "appt_duration"
    t.date     "appt_date"
    t.time     "appt_time"
    t.string   "username"
    t.integer  "doc_id"
    t.integer  "patient_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "bills", force: true do |t|
    t.integer  "bill_id"
    t.integer  "bill_amount"
    t.string   "payment_mode"
    t.integer  "discharge_id"
    t.integer  "appt_id"
    t.string   "username"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "discharges", force: true do |t|
    t.integer  "discharge_id"
    t.integer  "room_id"
    t.date     "admission_date"
    t.date     "discharge_date"
    t.string   "username"
    t.integer  "patient_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "doctors", force: true do |t|
    t.integer  "doc_id"
    t.string   "doc_name"
    t.string   "designation"
    t.text     "address"
    t.string   "doc_contact_num"
    t.integer  "fee"
    t.string   "username"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "logins", force: true do |t|
    t.string   "username"
    t.string   "password"
    t.string   "authorizedtype"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "metrics", force: true do |t|
    t.string   "metricname"
    t.integer  "metric_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "patients", force: true do |t|
    t.integer  "patient_id"
    t.string   "patient_name"
    t.string   "pat_contact_num"
    t.date     "date_of_birth"
    t.text     "address"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "username"
    t.string   "password_digest"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
