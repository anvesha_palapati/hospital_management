class CreateDischarges < ActiveRecord::Migration
  def change
    create_table :discharges do |t|
      t.integer :discharge_id
      t.integer :room_id
      t.date :admission_date
      t.date :discharge_date
      t.string :username
      t.integer :patient_id

      t.timestamps
    end
  end
end
