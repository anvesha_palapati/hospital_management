class CreateDoctors < ActiveRecord::Migration
  def change
    create_table :doctors do |t|
      t.integer :doc_id
      t.string :doc_name
      t.string :designation
      t.text :address
      t.string :doc_contact_num
      t.integer :fee
      t.string :username

      t.timestamps
    end
  end
end
