class CreatePatients < ActiveRecord::Migration
  def change
    create_table :patients do |t|
      t.integer :patient_id
      t.string :patient_name
      t.string :pat_contact_num
      t.date :date_of_birth
      t.text :address

      t.timestamps
    end
  end
end
