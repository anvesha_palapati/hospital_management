class CreateBills < ActiveRecord::Migration
  def change
    create_table :bills do |t|
      t.integer :bill_id
      t.integer :bill_amount
      t.string :payment_mode
      t.integer :discharge_id
      t.integer :appt_id
      t.string :username

      t.timestamps
    end
  end
end
