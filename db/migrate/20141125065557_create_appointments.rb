class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.integer :appt_id
      t.integer :appt_duration
      t.date :appt_date
      t.time :appt_time
      t.string :username
      t.integer :doc_id
      t.integer :patient_id
      t.string :username

      t.timestamps
    end
  end
end
