class Patient < ActiveRecord::Base
    
    has_many :appointments, dependent: :destroy
    has_many :doctors, dependent: :destroy
    has_one :discharge, dependent: :destroy
end
