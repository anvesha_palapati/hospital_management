class User < ActiveRecord::Base
    has_secure_password
    # attr_accessible :username, :password_digest, :password, :password_confirmation
    validates :username, :presence => true, :uniqueness => {:case_sensitive => false}
    validates :password, :length => { :in => 6..20}, :format => { :with => /\A[a-zA-Z0-9]+\z/, :message => "should not have special characters" }
end
