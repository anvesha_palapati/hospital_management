class Doctor < ActiveRecord::Base
    has_many :appointments, dependent: :destroy
    has_many :patients, dependent: :destroy
    belongs_to :login

end
