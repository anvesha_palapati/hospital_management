class Discharge < ActiveRecord::Base
    belongs_to :patient
    belongs_to :login
    has_one :bill, dependent: :destroy
end
