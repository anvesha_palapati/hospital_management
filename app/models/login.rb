class Login < ActiveRecord::Base
    has_one :doctor, dependent: :destroy
    has_one :discharge, dependent: :destroy
    has_one :appointment, dependent: :destroy
    has_one :bill, dependent: :destroy
end
