class UsersController < ApplicationController
    #force_ssl :only => [:new,:create]
    def new
        @user = User.new
    end
    
    def user_params
        params.require(:user).permit(:username, :password, :password_confirmation)
    end
    
    def create
        @user = User.new(user_params)
        if @user.save
            redirect_to root_url, :notice => "Successfully Signed up!"
            else
            render "new"
        end
    end
end
