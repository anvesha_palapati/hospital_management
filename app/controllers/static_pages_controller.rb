class StaticPagesController < ApplicationController

  def home
  end

  def about
  end
  
  def contact_us
  end
  
  def careers
  end
  
  def visiting
  end
  
  def sevices
  end
  
end