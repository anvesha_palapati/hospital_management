json.array!(@bills) do |bill|
  json.extract! bill, :id, :bill_id, :bill_amount, :payment_mode, :discharge_id, :appt_id, :username
  json.url bill_url(bill, format: :json)
end
