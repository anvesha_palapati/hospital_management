json.array!(@doctors) do |doctor|
  json.extract! doctor, :id, :doc_id, :doc_name, :designation, :address, :doc_contact_num, :fee, :username
  json.url doctor_url(doctor, format: :json)
end
