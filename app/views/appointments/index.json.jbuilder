json.array!(@appointments) do |appointment|
  json.extract! appointment, :id, :appt_id, :appt_duration, :appt_date, :appt_time, :username, :doc_id, :patient_id, :username
  json.url appointment_url(appointment, format: :json)
end
