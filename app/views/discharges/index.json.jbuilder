json.array!(@discharges) do |discharge|
  json.extract! discharge, :id, :discharge_id, :room_id, :admission_date, :discharge_date, :username, :patient_id
  json.url discharge_url(discharge, format: :json)
end
