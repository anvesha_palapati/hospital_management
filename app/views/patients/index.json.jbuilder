json.array!(@patients) do |patient|
  json.extract! patient, :id, :patient_id, :patient_name, :pat_contact_num, :date_of_birth, :address
  json.url patient_url(patient, format: :json)
end
