require 'test_helper'

class BillsControllerTest < ActionController::TestCase
  setup do
    @bill = bills(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:bills)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create bill" do
    assert_difference('Bill.count') do
      post :create, bill: { appt_id: @bill.appt_id, bill_amount: @bill.bill_amount, bill_id: @bill.bill_id, discharge_id: @bill.discharge_id, payment_mode: @bill.payment_mode, username: @bill.username }
    end

    assert_redirected_to bill_path(assigns(:bill))
  end

  test "should show bill" do
    get :show, id: @bill
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @bill
    assert_response :success
  end

  test "should update bill" do
    patch :update, id: @bill, bill: { appt_id: @bill.appt_id, bill_amount: @bill.bill_amount, bill_id: @bill.bill_id, discharge_id: @bill.discharge_id, payment_mode: @bill.payment_mode, username: @bill.username }
    assert_redirected_to bill_path(assigns(:bill))
  end

  test "should destroy bill" do
    assert_difference('Bill.count', -1) do
      delete :destroy, id: @bill
    end

    assert_redirected_to bills_path
  end
end
